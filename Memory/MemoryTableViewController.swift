import UIKit
import CoreData

class MemoryTableViewController: UITableViewController {
    var coreDataStack: CoreDataStack!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    class mainClass: UIViewController {
        @IBAction func scancode(_ sender: Any) {
            let solutionLogger = SolutionLogger(viewController: self)
            solutionLogger.scanQRCodeAndCaptureImage { (qrCode: String, image: UIImage)  in
            }
        }
    }
}

import UIKit
import AVFoundation

class SolutionLogger {
    let viewController: UIViewController
    
    init(viewController: UIViewController) {
        self.viewController = viewController
    }
    
    func logSolution<T: Encodable>(_ encodableEntry: T) {
        let logbookScheme = "appquest://submit"
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        guard let data = try? encoder.encode(encodableEntry),
            let solution = String(data: data, encoding: .utf8),
            let encodedSolution = solution.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed),
            let url = URL(string: "\(logbookScheme)/\(encodedSolution)") else {
                return
        }
        UIApplication.shared.open(url)
    }
    
    func scanQRCode(completion: @escaping (String) -> Void) {
        presentQRCodeReader { reader in
            reader.shouldCaptureImage = false
            reader.onCodeDetected = { code in
                reader.navigationController?.dismiss(animated: true) {
                    completion(code)
                }
            }
        }
    }
    
    func scanQRCodeAndCaptureImage(completion: @escaping (String, UIImage) -> Void) {
        presentQRCodeReader { reader in
            reader.shouldCaptureImage = true
            reader.onCodeDetectedAndImageCaptured = { code, img in
                reader.navigationController?.dismiss(animated: true) {
                    completion(code, img)
                }
            }
        }
    }
    
    private func presentQRCodeReader(configure: (QRCodeReaderViewController) -> ()) {
        let reader = QRCodeReaderViewController()
        let nc = UINavigationController(rootViewController: reader)
        nc.navigationBar.barTintColor = #colorLiteral(red: 0.04705882353, green: 0.3098039216, blue: 0.5647058824, alpha: 1)
        nc.navigationBar.isTranslucent = false
        nc.navigationBar.barStyle = .black
        configure(reader)
        viewController.present(nc, animated: true)
    }
}

fileprivate class QRCodeReaderViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate, AVCapturePhotoCaptureDelegate {
    var shouldCaptureImage = false
    let session: AVCaptureSession
    let previewLayer: AVCaptureVideoPreviewLayer
    let highlightView: UIView
    var photoOutput: AVCapturePhotoOutput?
    var detectedCode: String?
    var onCodeDetected: ((String) -> ())?
    var onCodeDetectedAndImageCaptured: ((String, UIImage) -> ())?
    
    init() {
        highlightView = UIView()
        highlightView.layer.borderColor = UIColor.green.cgColor
        highlightView.layer.borderWidth = 3
        session = AVCaptureSession()
        previewLayer = AVCaptureVideoPreviewLayer(session: session)
        super.init(nibName: nil, bundle: nil)
        
        self.title = "QR-Code-Scanner"
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancel))
        cancelButton.tintColor = #colorLiteral(red: 0.9932497144, green: 0.6978339553, blue: 0.2971298099, alpha: 1)
        self.navigationItem.leftBarButtonItem = cancelButton
    }
    
    @objc func cancel() {
        self.dismiss(animated: true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLayoutSubviews() {
        previewLayer.frame = view.bounds
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(highlightView)
        previewLayer.frame = self.view.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        self.view.bringSubview(toFront: highlightView)
        
        guard let device = AVCaptureDevice.default(for: .video),
            let videoInput = try? AVCaptureDeviceInput(device: device) else {
                return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        metadataOutput.setMetadataObjectsDelegate(self, queue: .main)
        
        if shouldCaptureImage {
            self.photoOutput = AVCapturePhotoOutput()
        }
        
        session.beginConfiguration()
        if session.canSetSessionPreset(.high) {
            session.sessionPreset = .high
        }
        session.addInput(videoInput)
        session.addOutput(metadataOutput)
        if shouldCaptureImage {
            session.addOutput(photoOutput!)
        }
        
        if metadataOutput.availableMetadataObjectTypes.contains(.qr) {
            metadataOutput.metadataObjectTypes = [.qr]
        }
        
        session.commitConfiguration()
        session.startRunning()
    }
    
    func metadataOutput(_ captureOutput: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        highlightView.frame = CGRect.zero
        
        for metadataObject in metadataObjects {
            guard metadataObject.type == .qr,
                let barCodeObject = previewLayer.transformedMetadataObject(for: metadataObject) as? AVMetadataMachineReadableCodeObject,
                let machineReadableObject = metadataObject as? AVMetadataMachineReadableCodeObject else {
                    continue
            }
            
            let detectionString = machineReadableObject.stringValue
            highlightView.frame = barCodeObject.bounds
            
            if detectionString != nil && detectedCode == nil {
                detectedCode = detectionString
                if shouldCaptureImage {
                    photoOutput?.capturePhoto(with: AVCapturePhotoSettings(), delegate: self)
                } else {
                    onCodeDetected?(detectedCode!)
                }
                return
            }
        }
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        guard error == nil else {
            print(error!.localizedDescription)
            return
        }
        
        if let data = photo.fileDataRepresentation(),
            let image = UIImage(data: data),
            let detectedCode = detectedCode {
            self.onCodeDetectedAndImageCaptured?(detectedCode, image)
            self.session.stopRunning()
        }
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        switch toInterfaceOrientation {
        case .portrait:
            previewLayer.connection?.videoOrientation = .portrait
        case .portraitUpsideDown:
            previewLayer.connection?.videoOrientation = .portraitUpsideDown
        case .landscapeLeft:
            previewLayer.connection?.videoOrientation = .landscapeLeft
        case .landscapeRight:
            previewLayer.connection?.videoOrientation = .landscapeRight
        default:
            previewLayer.connection?.videoOrientation = .portrait
        }
    }
}

